package com.celsoribeiro.githubauth.repositories;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;

import com.celsoribeiro.githubauth.data.RemoteRepository;
import com.celsoribeiro.githubauth.data.response.BranchListener;
import com.celsoribeiro.githubauth.data.response.CommitListener;
import com.celsoribeiro.githubauth.data.response.ReposListener;
import com.celsoribeiro.githubauth.model.Branch;
import com.celsoribeiro.githubauth.model.CommitModel;
import com.celsoribeiro.githubauth.model.Repos;

import java.util.List;

public class ReposPresenter implements ReposContract.Presenter {


    private final RemoteRepository repository;

    private ReposContract.View view;
    private Activity context;


    public ReposPresenter(Activity context) {
        this.repository = new RemoteRepository();
        this.context = context;
    }

    void takeView(ReposContract.View view) {
        this.view = view;
    }

    void dropView() {
        view = null;
    }

    @Override
    public void listRepos(String user) {
        repository.listRepos(user, new ReposListener() {
            @Override
            public void onSuccess(List<Repos> reposList) {
                if (view != null) {
                    if (reposList != null)
                        view.didListRepos(reposList);
                    else
                        view.didListReposFail();
                }
            }

            @Override
            public void onFailure() {
                if (view != null) {
                    view.didListReposFail();
                }
            }
        });
    }

    @Override
    public void getCommits(Repos repo) {
        repository.getCommits(repo, new CommitListener() {
            @Override
            public void onSuccess(List<CommitModel> list) {
                if (view != null) {
                    view.didGetCommits(list, repo);
                }
            }

            @Override
            public void onFailure() {
                if (view != null) {
                    view.didGetCommitsFail();
                }
            }
        });
    }

    @Override
    public void getBranches(Repos repo) {
        repository.getBranches(repo.getId(), new BranchListener() {
            @Override
            public void onSuccess(List<Branch> list) {
                if (view != null) {
                    view.didGetBranches(list, repo);
                }
            }

            @Override
            public void onFailure() {
                if (view != null) {
                    view.didGetBranchesFail();
                }
            }
        });
    }

    public boolean isOnline(Activity activity) {
        ConnectivityManager manager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return manager.getActiveNetworkInfo() != null &&
                manager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

}
