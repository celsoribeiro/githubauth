package com.celsoribeiro.githubauth.data;

import com.celsoribeiro.githubauth.model.Branch;
import com.celsoribeiro.githubauth.model.CommitModel;
import com.celsoribeiro.githubauth.model.Repos;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface GitHubApi {

    String ROOT_ENDPOINT = "https://api.github.com/";
    String ACESS_TOKEN = "f1600d87e325766c0ef8a7e23175b762883c1bc7";

    @Headers("Authorization: token " + ACESS_TOKEN)
    @GET("users/{user}/repos")
    Call<List<Repos>> listRepos(@Path("user") String user);

    @Headers("Authorization: token " + ACESS_TOKEN)
    @GET("/repos/{owner}/{repo}/commits")
    Call<List<CommitModel>> getCommits(@Path("owner") String owner, @Path("repo") String repo);

    @Headers("Authorization: token " + ACESS_TOKEN)
    @GET("repositories/{repo}/branches")
    Call<List<Branch>> getBranches(@Path("repo") String repo);

   /* @Headers("Authorization: token " + ACESS_TOKEN)
    @GET("/repos/{owner}/{repo}/commits/{sha}")
    Call<CommitModel> getCommit(@Path("owner") String owner, @Path("repos") String repo, @Path("sha") String commit_sha);*/
}
