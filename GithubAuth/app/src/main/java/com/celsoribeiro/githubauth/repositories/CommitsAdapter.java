package com.celsoribeiro.githubauth.repositories;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.celsoribeiro.githubauth.R;
import com.celsoribeiro.githubauth.model.CommitModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CommitsAdapter extends RecyclerView.Adapter<CommitsAdapter.ItemViewHolder> {

    private List<CommitModel> list;


    public CommitsAdapter(List<CommitModel> list) {
        this.list = list;

    }

    @Override
    public int getItemCount() {
        if (list.size() > 5)
            return 5;

        return list.size();
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder ItemViewHolder, final int i) {

        final CommitModel commitModel = list.get(i);
        ItemViewHolder.commitName.setText(commitModel.getCommit().getMessage());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        SimpleDateFormat formatOut = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        try {
            Date date = format.parse(commitModel.getCommit().getAuthor().getDate());
            String dateTime = formatOut.format(date);
            ItemViewHolder.commitDate.setText(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ItemViewHolder itemViewHolderObject;
        View itemView;
        int res = R.layout.commit_adapter_layout;

        itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(res, viewGroup, false);
        itemViewHolderObject = new ItemViewHolder(itemView);
        return itemViewHolderObject;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        protected TextView commitName, commitDate;

        public ItemViewHolder(View v) {
            super(v);

            commitName = (TextView) v.findViewById(R.id.commitName);
            commitDate = (TextView) v.findViewById(R.id.commitDate);


        }
    }


}
