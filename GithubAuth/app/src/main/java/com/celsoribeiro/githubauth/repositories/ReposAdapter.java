package com.celsoribeiro.githubauth.repositories;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.celsoribeiro.githubauth.R;
import com.celsoribeiro.githubauth.model.CommitModel;
import com.celsoribeiro.githubauth.model.Repos;

import java.util.List;

public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.ItemViewHolder> {

    private List<Repos> list;
    private Context context;

    public ReposAdapter(List<Repos> list, Context cont) {
        this.list = list;
        this.context = cont;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder ItemViewHolder, final int i) {

        final Repos repos = list.get(i);
        ItemViewHolder.reposName.setText(repos.getName());
        ItemViewHolder.reposName.setPaintFlags(ItemViewHolder.reposName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        ItemViewHolder.reposName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(repos.getHtml_url())));
            }
        });

        if (repos.getCommits() != null) {
            List<CommitModel> commits = repos.getCommits();
            ItemViewHolder.commitRecycler.setAdapter(new CommitsAdapter(commits));
        }

    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ItemViewHolder itemViewHolderObject;
        View itemView;
        int res = R.layout.adapter_layout;

        itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(res, viewGroup, false);
        itemViewHolderObject = new ItemViewHolder(itemView);
        return itemViewHolderObject;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        protected TextView reposName;
        protected RecyclerView commitRecycler;

        public ItemViewHolder(View v) {
            super(v);
            reposName = (TextView) v.findViewById(R.id.reposName);

            commitRecycler = (RecyclerView) v.findViewById(R.id.commitRecycler);
            commitRecycler.setHasFixedSize(true);
            commitRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));


        }
    }

}
