package com.celsoribeiro.githubauth.data.response;


import com.celsoribeiro.githubauth.model.CommitModel;

import java.util.List;


public interface CommitListener {

    void onSuccess(List<CommitModel> reposList);

    void onFailure();

}
