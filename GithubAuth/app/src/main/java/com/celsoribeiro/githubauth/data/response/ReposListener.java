package com.celsoribeiro.githubauth.data.response;


import com.celsoribeiro.githubauth.model.Repos;

import java.util.List;


public interface ReposListener {

    void onSuccess(List<Repos> reposList);

    void onFailure();

}
