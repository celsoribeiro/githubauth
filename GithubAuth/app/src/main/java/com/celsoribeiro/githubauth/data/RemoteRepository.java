package com.celsoribeiro.githubauth.data;


import com.celsoribeiro.githubauth.data.response.BranchListener;
import com.celsoribeiro.githubauth.data.response.CommitListener;
import com.celsoribeiro.githubauth.data.response.ReposListener;
import com.celsoribeiro.githubauth.model.Branch;
import com.celsoribeiro.githubauth.model.CommitModel;
import com.celsoribeiro.githubauth.model.Repos;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RemoteRepository {

    private GitHubApi gitHubApi;


    public RemoteRepository() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GitHubApi.ROOT_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        gitHubApi = retrofit.create(GitHubApi.class);

    }


    public void listRepos(String user, final ReposListener listener) {
        gitHubApi.listRepos(user).enqueue(new Callback<List<Repos>>() {
            @Override
            public void onResponse(Call<List<Repos>> call, Response<List<Repos>> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Repos>> call, Throwable t) {
                listener.onFailure();
            }
        });

    }

    public void getBranches(String repo, final BranchListener listener) {
        gitHubApi.getBranches(repo).enqueue(new Callback<List<Branch>>() {
            @Override
            public void onResponse(Call<List<Branch>> call, Response<List<Branch>> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Branch>> call, Throwable t) {
                listener.onFailure();
            }
        });

    }

    public void getCommits(Repos repos, final CommitListener listener) {
        gitHubApi.getCommits(repos.getOwner().getLogin(), repos.getName()).enqueue(new Callback<List<CommitModel>>() {
            @Override
            public void onResponse(Call<List<CommitModel>> call, Response<List<CommitModel>> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<CommitModel>> call, Throwable t) {
                listener.onFailure();
            }
        });

    }


}
