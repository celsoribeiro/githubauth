package com.celsoribeiro.githubauth.repositories;


import com.celsoribeiro.githubauth.model.Branch;
import com.celsoribeiro.githubauth.model.CommitModel;
import com.celsoribeiro.githubauth.model.Repos;

import java.util.List;


public interface ReposContract {

    interface View {


        void didListRepos(List<Repos> list);

        void didListReposFail();

        void didGetCommits(List<CommitModel> list, Repos repo);

        void didGetCommitsFail();

        void didGetBranches(List<Branch> list, Repos repo);

        void didGetBranchesFail();


    }

    interface Presenter {

        void listRepos(String user);

        void getCommits(Repos repo);

        void getBranches(Repos repo);

    }

}
