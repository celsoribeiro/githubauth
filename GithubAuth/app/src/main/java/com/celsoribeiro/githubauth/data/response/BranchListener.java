package com.celsoribeiro.githubauth.data.response;


import com.celsoribeiro.githubauth.model.Branch;

import java.util.List;


public interface BranchListener {

    void onSuccess(List<Branch> reposList);

    void onFailure();

}
