package com.celsoribeiro.githubauth.repositories;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.celsoribeiro.githubauth.R;
import com.celsoribeiro.githubauth.model.Branch;
import com.celsoribeiro.githubauth.model.CommitModel;
import com.celsoribeiro.githubauth.model.Repos;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReposActivity extends AppCompatActivity implements ReposContract.View {

    ProgressDialog dialog;

    @BindView(R.id.recycleView)
    RecyclerView recyclerView;

    ReposPresenter presenter;
    List<Repos> repositories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repos);
        ButterKnife.bind(this);
        presenter = new ReposPresenter(this);

        dialog = new ProgressDialog(ReposActivity.this);
        dialog.setMessage("Carregando...");
        dialog.setCancelable(false);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        //recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        dialog.show();

        String username = getIntent().getStringExtra("username");
        setTitle(username);
        presenter.listRepos(username);


    }

    @Override
    public void didListRepos(List<Repos> list) {
        dialog.dismiss();
        repositories = list;
        ReposAdapter reposAdapter = new ReposAdapter(repositories, this);
        recyclerView.setAdapter(reposAdapter);

        dialog.show();

        for (Repos repos : repositories) {
            presenter.getCommits(repos);
        }

    }

    @Override
    public void didListReposFail() {
        dialog.dismiss();

    }

    @Override
    public void didGetBranches(List<Branch> list, Repos repos) {
        dialog.dismiss();
        presenter.getCommits(repos);
    }

    @Override
    public void didGetBranchesFail() {
        dialog.dismiss();
    }

    @Override
    public void didGetCommits(List<CommitModel> list, Repos repos) {
        int position = repositories.indexOf(repos);
        repositories.get(position).setCommits(list);
        recyclerView.getAdapter().notifyItemChanged(position);
        dialog.dismiss();

    }

    @Override
    public void didGetCommitsFail() {
        dialog.dismiss();
    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onPause() {
        presenter.dropView();
        super.onPause();
    }


}
