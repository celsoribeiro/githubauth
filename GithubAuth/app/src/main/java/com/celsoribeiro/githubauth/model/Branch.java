package com.celsoribeiro.githubauth.model;

import com.google.gson.annotations.SerializedName;

public class Branch {
    @SerializedName("protected")
    private boolean _protected;

    private String name;

    private CommitInfo commit;

    public boolean is_protected() {
        return _protected;
    }

    public void set_protected(boolean _protected) {
        this._protected = _protected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CommitInfo getCommit() {
        return commit;
    }

    public void setCommit(CommitInfo commit) {
        this.commit = commit;
    }


}
			