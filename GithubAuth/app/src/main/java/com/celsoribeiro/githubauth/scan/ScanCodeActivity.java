package com.celsoribeiro.githubauth.scan;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.celsoribeiro.githubauth.R;
import com.celsoribeiro.githubauth.repositories.ReposActivity;
import com.google.zxing.Result;

public class ScanCodeActivity extends AppCompatActivity {

    private CodeScanner mCodeScanner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        setTitle("Scan your QR Code");
        if (hasPermission()) {
            readyScan();
        } else {
            askPermission();
        }

    }

    private void readyScan() {

        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                ScanCodeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String username = result.getText().replace("https://github.com/", "");
                        Intent intent = new Intent(ScanCodeActivity.this, ReposActivity.class);
                        intent.putExtra("username", username);
                        startActivity(intent);
                    }
                });

            }
        });

        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {

        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            readyScan();

        } else {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                boolean showRationale = shouldShowRequestPermissionRationale(permissions[0]);
                if (showRationale)
                    showAlert();
            } else {
                showAlert();
            }

        }
        return;

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCodeScanner != null)
            mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        if (mCodeScanner != null)
            mCodeScanner.releaseResources();
        super.onPause();
    }

    private void askPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                0);
    }

    public boolean hasPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    private void showAlert() {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("You need to allow this action to continue using the app");
        builder.setPositiveButton("Ask me again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                askPermission();
            }
        });
        builder.setNegativeButton("Leave", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });
        android.app.AlertDialog alert = builder.create();

        if (!((Activity) this).isFinishing()) {
            alert.show();
        }

    }
}
